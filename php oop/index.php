<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold_blooded: " . $sheep->cold_blooded; // "no"

echo "<p>";


$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // "shaun"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold_blooded: " . $kodok->cold_blooded . "<br>"; // "no"
$kodok->jump(); // "hop hop"

echo "<p>";


$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // "shaun"
echo "legs : " . $sungokong->legs . "<br>"; // 4
echo "cold_blooded: " . $sungokong->cold_blooded . "<br>"; // "no"
$sungokong->yell(); // "Auooo"