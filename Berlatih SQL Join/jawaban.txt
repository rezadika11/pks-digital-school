 
Soal 1 Mengambil Data dari Database
a. Mengambil data users
   SELECT id,name,email FROM users;
b. Mengambil data items
   - Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
     SELECT * FROM items WHERE price > 1000000;
   - Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci                   “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
   SELECT * FROM items WHERE name LIKE '%watch';
c. Menampilkan data items join dengan kategori
   SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories WHERE items.category_id = categories.id;

Soal 2 Mengubah Data dari Database
    UPDATE items SET price= 2500000 WHERE id=1;

